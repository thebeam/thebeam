import { h, render, Component } from 'preact';
import style from './style';

class Home extends Component {
	state = {
		cursorCoords: [0,0],
		serverCoords: [0,0],
		status: 'disconnected'
	}
	socketCommsListeners(socket) {
		window.socket.addEventListener('close', (e) => {
			console.log('onclose');
			this.setState({
				status: 'closed'
			});
			this.teardownSocket(window.socket);
		});

		window.socket.addEventListener('open', (e) => {
			console.log('onclose')
			this.setState({
				status: "connected"
			});
		});
		window.socket.addEventListener('error', (e) => {
			console.log('onerror');
			console.log(e);
			this.setState({
				status: 'error'
			});
			// this.teardownSocket(window.socket);
		});
		window.socket.addEventListener('message', (e) => {
			let serverCoords = e.data.split(',');
			console.log([parseInt(serverCoords[0]), parseInt(serverCoords[1]) ]);
			
			this.setState({
				serverCoords: [parseInt(serverCoords[0]), parseInt(serverCoords[1]) ]
			});
		});
	}

	teardownSocket = async (socket) => {

		window.socket.close();
		window.socket = null;
		console.log('completed teardown of socket');
		const reconnectResult = await this.awaitWebsocketConnection();
		console.log(`teardown reconnect result: ${reconnectResult}`);
	}

	awaitWebsocketConnection() {
		return new Promise(async (resolve, reject) => {
			let intervalId;
			let initiationInProgress = false;

			intervalId = setInterval(async () => {
				if (window.socket) {
					clearInterval(intervalId);
					resolve('Socket is live, clearing interval.');
					return;
				}
				if (!window.socket) {
					if (initiationInProgress) return;
					if (!initiationInProgress) {
						initiationInProgress = true;
						initiationInProgress = await this.initiateWebSocketConnection();

						if (initiationInProgress === 'open') {
							clearInterval(intervalId);
							resolve("open");
						}
					}
					
				}

			}, 5000);

		});
	}

	awaitConnectionResults() {
		return new Promise(async (resolve, reject) => {
			const intervalId = setInterval(() => {
				let status;
				try {
					status = window.socket.readyState;
				} catch(e) {
					if (!window.socket) {
						clearInterval(intervalId);
						reject('no-ready-state');
					};

				}
				console.log('Checking connection status', status);
				if (status === 0) return;
				let resp;
				switch (status) {
					case 1:
						resp = 'open';
						break;
					case 2:
						resp = 'closing';
						break;
					case 3:
						resp = 'closed';
						break;
					default:
						resp = `unexpected-${status}`;
				}
				clearInterval(intervalId);
				resolve(resp);
			}, 500);
		});
	}
	installCursorListeners() {
		let t = this;

		let samplingIdx = 0;
		function handleMouseMove(event) {
			samplingIdx++;
			if (samplingIdx === 2) {
			  samplingIdx = 0;
			  return;
			}
			if (samplingIdx !== 1) return;
			let eventDoc, doc, body;
			console.log('mouse move');
	
			event = event || window.event; // IE-ism
	
			// If pageX/Y aren't available and clientX/Y are,
			// calculate pageX/Y - logic taken from jQuery.
			// (This is to support old IE)
			if (event.pageX === null && event.clientX !== null) {
				eventDoc = (event.target && event.target.ownerDocument) || document;
				doc = eventDoc.documentElement;
				body = eventDoc.body;
	
				event.pageX = event.clientX +
				  (doc && doc.scrollLeft || body && body.scrollLeft || 0) -
				  (doc && doc.clientLeft || body && body.clientLeft || 0);
				event.pageY = event.clientY +
				  (doc && doc.scrollTop  || body && body.scrollTop  || 0) -
				  (doc && doc.clientTop  || body && body.clientTop  || 0 );
			}
	
			t.setState({
				cursorCoords: [event.pageX, event.pageY]
			});
			// Use event.pageX / event.pageY here
		}

		document.onmousemove = handleMouseMove;
	}
	initiateWebSocketConnection(){
		return new Promise(async (resolve, reject) => {
			this.setState({
				status: 'connecting'
			});
			window.socket = new WebSocket('ws://localhost:1111/websockets');
			this.socketCommsListeners();
			const connectionResult = await this.awaitConnectionResults(window.socket);
			console.log(`got connectionSTatus ${connectionResult}`);
			if (connectionResult === 'open') {
				resolve('open');
				this.installCursorListeners();
			}
			if (!connectionResult === 'open') resolve('close');
		});

	}
	componentDidMount() {
		this.initiateWebSocketConnection();
	}

	shouldComponentUpdate(nextProps, nextState)	 {
		if (nextState.cursorCoords !== this.state.cursorCoords) {
			if (window.socket.readyState !== 0) {

				window.socket.send(nextState.cursorCoords);
			}
		}
	}

	// Lifecycle: Called just before our component will be destroyed
	componentWillUnmount() {
		// stop when not renderable
		document.onmousemove = null;
	}
	renderCursor() {
		return (
			<div className="cursorWrap">
				<i className="fas fa-mouse-pointer" style={{
					left: this.state.serverCoords[0]+'px',
					top: this.state.serverCoords[1]+'px',
					fontSize: "20px",
					position: "absolute",
					animation: "all 0.3s ease-in"
				}} />
			</div>
		);
	}
	render() {
		let cl = {
			width:"100%",
			height:"100%"
		};
		if (this.state.status === 'connected') cl['cursor'] = 'none';
		return (
			<div style={cl}>
				<script src="https://kit.fontawesome.com/67d6c77b60.js" crossorigin="anonymous" />

				<h1>Home</h1>
				<h3>Status: {this.state.status}</h3>
				<h4>Local Coords</h4>
				{this.state.cursorCoords[0]}
				<br />
				{this.state.cursorCoords[1]}
				<h4>Server Coords</h4>
				{this.state.serverCoords[0]}
				<br />
				{this.state.serverCoords[1]} 
				{this.renderCursor()}
			</div>
		);
	}
}

export default Home;
