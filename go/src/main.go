package main

import (
	"fmt"
	"net/http"
	"github.com/gorilla/websocket"
	"log"
)

// This upgrades the HTTP connection to a websocket.
var upgrader = websocket.Upgrader{
    ReadBufferSize:  1024,
    WriteBufferSize: 1024,
}

// define a reader which will listen for
// new messages being sent to our WebSocket
// endpoint
func reader(conn *websocket.Conn) {
    for {
    // read in a message
        messageType, p, err := conn.ReadMessage()
        if err != nil {
            log.Println(err)
            return
        }
    // print out that message for clarity
        fmt.Println(string(p))

        if err := conn.WriteMessage(messageType, p); err != nil {
            log.Println(err)
            return
        }

    }
}

func main() {
	http.HandleFunc("/websockets", func(responseWriter http.ResponseWriter, webRequest *http.Request) {
		// takes in the Request, responds with a bool of true
		upgrader.CheckOrigin = func(r *http.Request) bool { 
			return true 
		}

		log.Println("connection");
		// upgrade this connection to a WebSocket
		// connection
		ws, err := upgrader.Upgrade(responseWriter, webRequest, nil)
		if err != nil {
			log.Println(err, "error")
		}

		// helpful log statement to show connections
		log.Println("Client Connected")

		err = ws.WriteMessage(1, []byte("Hi Client!"))
		reader(ws)
    })

	http.ListenAndServe(":1111", nil)
}